const graphql = require('graphql');
const fetch = require('node-fetch');
const { GraphQLObjectType, GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString, GraphQLSchema } = graphql;
const OrganizationType = require('./types').OrganizationType;
const ContributorsType = require('./types').ContributorsType;
const FollowersType = require('./types').FollowersType;
const ProjectsType = require('./types').ProjectsType;


const BASE_URL = "https://api.github.com/";
function fetchResponseByURL(relativeURL) {
    return fetch(`${BASE_URL}${relativeURL}?access_token=bfde28d41d143ce77f9220daf1cd2676896c5c6e&per_page=100`)
    .then(res => res.json())
    .catch(err => err.json());
}

const RootQuery = new GraphQLObjectType({
    name: "RootQuery",
    fields: ()=>({
        Organization: {
            type: OrganizationType,
            args: {
                login: {type: GraphQLString}
            },
            resolve: (parentValue, args) => {
                return fetchResponseByURL(`orgs/${args.login}`).then(data=>data);
            }
        },
        AngularContributors: {
            type: new GraphQLList(ContributorsType),
            resolve: ()=>{
                return fetchResponseByURL(`orgs/angular/members`).then(data=>data);
            }
        },
        Contributor: {
            type: ContributorsType,
            args: {
                login: {type: GraphQLString}
            },
            resolve: (parentValue, args) => {
                return fetchResponseByURL(`users/${args.login}`).then(data=>data);
            }
        },
        Project: {
            type: ProjectsType,
            args: { 
                login: {type: GraphQLString},
                name: {type: GraphQLString}                
            },
            resolve: (parentValue, args) => {
                return fetchResponseByURL(`repos/${args.login}/${args.name}`)
            }   
        },
        Followers: {
            type: new GraphQLList(FollowersType),
            resolve: ()=>{
                return fetchResponseByURL(`users`).then(data=>data);
            }

        }
    })
});

module.exports = new GraphQLSchema({
    query: RootQuery
});