const graphql = require('graphql');
const fetch = require('node-fetch');
const { 
    GraphQLObjectType, 
    GraphQLString, 
    GraphQLID, 
    GraphQLList, 
    GraphQLInt, 
    GraphQLNonNull } = graphql;

    const BASE_URL = "https://api.github.com/";
    function fetchResponseByURL(relativeURL) {
        return fetch(`${BASE_URL}${relativeURL}?access_token=bfde28d41d143ce77f9220daf1cd2676896c5c6e&per_page=100`)
        .then(res => res.json())
        .catch(err => err.json())
    }
    

//organization type
const OrganizationType = new GraphQLObjectType({
    name: "Organization",
    fields: ()=>({
        login: {
            type: GraphQLString,
            resolve: organization => organization.login},
        name: {
            type: GraphQLString,
            resolve: organization => organization.name},
        website: {
            type: GraphQLString,
            resolve: organization => organization.blog},
        avatar: {
            type: GraphQLString,
            resolve: organization => organization.avatar_url},
        NoOfRepos: {
            type: GraphQLInt,
            resolve: organization => organization.public_repos},
        Contributors: {
            type: new GraphQLList(ContributorsType),
            resolve: parentValue => {
                return fetchResponseByURL(`orgs/${parentValue.login}/members`).then(data=>data);
            }
        },
        createDate:{
            type: GraphQLString,
            resolve: organization => organization.created_at}
    })
});

const ContributorsType = new GraphQLObjectType({
    name: "Contributors",
    fields: ()=>({
        login: {
            type: GraphQLString,
            resolve: contributor => contributor.login},
        id: {
            type: GraphQLID,
            resolve: contributor => contributor.id},
        name: {
            type: GraphQLID,
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}`).then(data=>data.name);
            }
        },
        avatar: {
            type: GraphQLString,
            resolve: contributor => contributor.avatar_url},
        url: {
            type: GraphQLString,
            resolve: contributor => contributor.html_url},
        followersNum: {
            type: GraphQLInt,
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}`).then(data=>data.followers);
            }
        },
        projectsNum: {
            type: GraphQLInt,
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}`).then(data=>data.public_repos);
            }
        },
        gistsNum: {
            type: GraphQLInt,
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}`).then(data=>data.public_gists);
            }
        },
        followers: {
            type: new GraphQLList(FollowersType),
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}/followers`).then(data=>data);
            }
        },
        projects:{
            type: new GraphQLList(ProjectsType),
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}/repos`).then(data=>data);
            }
        },
        gists: {
            type: new GraphQLList(GistsType),
            resolve: parentValue => {
                return fetchResponseByURL(`users/${parentValue.login}/gists`).then(data=>data);
            }
        }  
    })
})

const FollowersType = new GraphQLObjectType({
    name: "Followers",
    fields: ()=>({
        login: {
            type: GraphQLString,
            resolve: followers => followers.login},
        id: {
            type: GraphQLID,
            resolve: followers => followers.id},
        avatar: {
            type: GraphQLString,
            resolve: followers => followers.avatar_url},
        url: {
            type: GraphQLString,
            resolve: followers => followers.html_url}
    })
})

const ProjectsType = new GraphQLObjectType({
    name: "Projects",
    fields: ()=>({
        id: {
            type: GraphQLID,
            resolve: project => project.id},
        name: {
            type: GraphQLString,
            resolve: project => project.name},
        full_name: {
            type: GraphQLString,
            resolve: project => project.full_name
        },
        description:{
            type: GraphQLString,
            resolve: project => project.description
        },
        owner: {
            type: GraphQLString,
            resolve: project => project.owner.login
        },
        otherContributors:{
            type: new GraphQLList(ContributorsType),
            resolve: parentValue => {
                return fetchResponseByURL(`repos/${parentValue.owner.login}/${parentValue.name}/contributors`).then(data=>data);
            } 
        },
        commits:{
            type: new GraphQLList(CommitsType),
            resolve: parentValue => {
                return fetchResponseByURL(`repos/${parentValue.owner.login}/${parentValue.name}/commits`).then(data=>data);
            } 

        }
    })
}) 
 const GistsType = new GraphQLObjectType({
    name: "Gists",
    fields: ()=>({
        id: {
            type: GraphQLID,
            resolve: gist => gist.id}
    })
}) 
const CommitsType = new GraphQLObjectType({
   name: "Commits",
   fields: ()=>({
       id: {
           type: GraphQLString,
           resolve: commit => commit.sha}
   })
}) 

module.exports =  {OrganizationType, ContributorsType, FollowersType, ProjectsType, GistsType};