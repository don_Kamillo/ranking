const express = require('express');
const expressGraphQL = require('express-graphql');
const webpackMiddleware = require('webpack-dev-middleware');
const webpack = require('webpack');
const webpackConfig = require('../webpack.config.js');
const schema = require('./schema/schema')

const app = express();
app.use('/graphql', expressGraphQL({
  schema,
  graphiql: true
}));


app.use(webpackMiddleware(webpack(webpackConfig)));

module.exports = app;