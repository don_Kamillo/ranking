import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
//Components 
import App from './components/App';
import Main from './components/Main.js';
import ContributorsPage from './components/ContributorsPage.js';
import RepositoriesPage from './components/RepositoriesPage.js';


const networkInterface = createNetworkInterface({
    uri:'/graphql',
    opts:{
      credentials: 'same-origin'
    }
  })

const client = new ApolloClient({
    networkInterface,
    dataIdFromObject: o => o.id
  });

  const Root = () => {
    return (
          <ApolloProvider client={client}>
            <Router history={hashHistory}>
              <Route path="/" component={App}>
                <IndexRoute component={Main}/>
                <Route path="contributors/:login" component={ContributorsPage}></Route>
                <Route path="repositories/:login/:name" component={RepositoriesPage}></Route>
              </Route>
            </Router>
          </ApolloProvider>
    );
  };


ReactDOM.render(<Root />, document.getElementById('root'));