import React from 'react';
import {Link} from 'react-router';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import {  Icon, Label, Menu, Table, Button, 
          TableBody, TableRow, TableHeader, 
          TableCell, TableHeaderCell, Grid, 
          Image, Container, Segment, Header,
          Dimmer, Loader, Card, List,Statistic } from 'semantic-ui-react'


class ContributorsPage extends React.Component {
    projectsList(){
      return this.props.data.Contributor.projects.map((project,index)=>{
        return(
          <Label key={index}>
            <Link to={`repositories/${this.props.params.login}/${project.name}`}>
              {project.name}
            </Link>
          </Label>
        )
      })
    };
    render(){
      if(this.props.data.loading){
        return(<div/>)
      }
      else{
        console.log(this.props.data);
        const {name, avatar, login, followersNum, projectsNum, gistsNum, projects} = this.props.data.Contributor;

        
        
        // const commitsNum = projects.commits.length;
        return(
          <div>
            <Grid columns={3}>
              <Grid.Row>
              <Grid.Column>
              <Statistic>
                <Statistic.Value>{followersNum}</Statistic.Value>
                <Statistic.Label>followers</Statistic.Label>
              </Statistic>
              <Statistic>
                <Statistic.Value>{projectsNum}</Statistic.Value>
                <Statistic.Label>repositories</Statistic.Label>
              </Statistic>
              <Statistic>
                <Statistic.Value>{gistsNum}</Statistic.Value>
                <Statistic.Label>gists</Statistic.Label>
              </Statistic>
              </Grid.Column>
              <Grid.Column>
                <Card centered raised>
                  <Image src={avatar} />
                  <Card.Content>
                    <Card.Header>
                      {name}
                    </Card.Header>
                    <Card.Meta>
                      {login}
                    </Card.Meta>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Header>Other {name}'s repositories</Header>
                {this.projectsList()}
              </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        )
      }
    }
};
const query = gql`
query fetchCon($ConID: String){
  Contributor(login:$ConID){
    login
    avatar
    name
    projects{
      name

    }
    projectsNum
    followersNum
    gistsNum
  }
}
`;
export default graphql(query,{
  options:(props)=>{
    return {variables:{
      ConID: props.params.login
    }}
  }
})(ContributorsPage);


/* commits{
        id
      } */