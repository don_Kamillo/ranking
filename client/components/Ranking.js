import _ from 'lodash';
import React, { Component } from 'react';
import {Link} from 'react-router';
import {  Icon, Label, Menu, Table, Button, 
          TableBody, TableRow, TableHeader, 
          TableCell, TableHeaderCell, Grid, 
          Image, Container, Segment, Header,
          Dimmer, Loader } from 'semantic-ui-react'
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';


class Ranking extends React.Component {    
    constructor(props){
      super(props);

      this.state = {
        column: null,
        data: null,
        direction: null,
      }
    };

    componentWillReceiveProps(nextProps){
      this.setState({
        data: nextProps.data.AngularContributors
      })
    }
    handleSort = clickedColumn => () => {
      const { column, data, direction } = this.state
  
      if (column !== clickedColumn) {
        this.setState({
          column: clickedColumn,
          data: _.sortBy(data, [function(d) { return d[clickedColumn]; }]),
          direction: 'ascending',
        })
        // console.log(data)
        return
      }
        this.setState({
          data: data.reverse(),
          direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    };
    handleSortAlpha = clickedColumn => () => {
      const { column, data, direction } = this.state
  
      if (column !== clickedColumn) {
        this.setState({
          column: clickedColumn,
          data: _.sortBy(data, [function(d) { return d[clickedColumn].toLowerCase(); }]),
          direction: 'ascending',
        })
        return
      }
        this.setState({
          data: data.reverse(),
          direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    };

    render(){
      if(this.props.data.loading){
        return(
          <Loader active inline='centered' />
        )
      }
      else {
        const { column, data, direction } = this.state
        // console.log(this.props,this.state);
        return(
            <Table 
              sortable 
              basic >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell sorted={column === 'login' ? direction : null} onClick={this.handleSortAlpha('login')}>
                  Name
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'followers' ? direction : null} onClick={this.handleSort('followersNum')}>
                  Followers
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'projects' ? direction : null} onClick={this.handleSort('projectsNum')}>
                  Projects
                </Table.HeaderCell>
                <Table.HeaderCell sorted={column === 'gists' ? direction : null} onClick={this.handleSort('gistsNum')}>
                  Gists
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {_.map(data, ({ id, avatar, login, name, followersNum, projectsNum, gistsNum }) => (
                <Table.Row key={id}>
                <Table.Cell><Link to={`contributors/${login}`}>{`${name} (${login})`}</Link></Table.Cell>
                  <Table.Cell>{followersNum}</Table.Cell>
                  <Table.Cell>{projectsNum}</Table.Cell>
                  <Table.Cell>{gistsNum}</Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
      )
      }
    }
};
const query = gql`
{
	AngularContributors{
    id
    login
    name
    avatar
    followersNum
    projectsNum
    gistsNum
  }
}
`;
export default graphql(query)(Ranking);