import _ from 'lodash';
import React, { Component } from 'react';
import {Link} from 'react-router';
import {  Icon, Label, Menu, Table, Button, 
          TableBody, TableRow, TableHeader, 
          TableCell, TableHeaderCell, Grid, 
          Image, Container, Segment, Header,
          Dimmer, Loader, Statistic, Sticky, Rail } from 'semantic-ui-react'
import { graphql, compose  } from 'react-apollo';
import gql from 'graphql-tag';
import Ranking from './Ranking';


class Main extends React.Component {
    render(){
      if(this.props.data.loading){
        return(<div/>)
      }
      else {
        // console.log(this.props,this.state);
        const { name, website, login, NoOfRepos, createDate, Contributors } = this.props.data.Organization;
        const year = new Date(createDate).getFullYear();
        return(
        <div>
        <Grid columns={2}>
          <Grid.Column 
            color='black' 
            width={6}>
              <Image 
                src='https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/2000px-Angular_full_color_logo.svg.png'
                href={website}
                size="huge" />
          </Grid.Column>
          <Grid.Column 
            color='black'
            width={10}>
              <Grid.Row>
                </Grid.Row>
                <Header 
                  size="huge" 
                  inverted> </Header>
                <Statistic inverted>
                  <Statistic.Value>{NoOfRepos}</Statistic.Value>
                  <Statistic.Label>Repositories</Statistic.Label>
                </Statistic >
                <Statistic inverted>
                  <Statistic.Value>{Contributors.length}</Statistic.Value>
                  <Statistic.Label>Contributors</Statistic.Label>
                </Statistic>
                <Statistic inverted>
                  <Statistic.Value>{year}</Statistic.Value>
                  <Statistic.Label>Helping since</Statistic.Label>
                </Statistic>
                <Header size="large" inverted>{`Top frontend framework. 
                    Not all heros wear capes, but below 
                    you can find those, who created Angular. 
                    And this is something.`}
                </Header>
          </Grid.Column>
        </Grid>
        <Grid>
          <Grid.Row centered  color="black">
          <a href='#Ranking'>
              <Icon name="chevron down" size="huge" />
          </a>
          </Grid.Row>
        </Grid>
        <Grid container>
          <Ranking/>
        </Grid>
        </div>
      )
      }
    }
};
const query = gql`
{
  Organization(login:"Angular"){
    login
    website
    name
    NoOfRepos
    createDate
    Contributors{
      id
    }
  }
}
`;
export default graphql(query)(Main);