import React from 'react';
import {Link} from 'react-router';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

class RepositoriesPage extends React.Component  {
    constructor(props){
      super(props);
    };
    contributorsList(){
      return this.props.data.Project.otherContributors.map((user,index)=>{
        return(
          <li key={index}>
            <Link to={`contributors/${user.login}`}>{user.login}</Link>
          </li>
        )
      })
    };
    render(){
      if(this.props.data.loading){
        return(<div/>)
      }
      else if(!this.props.data.loading){
        console.log(this.props);
        return(
          <div>
            <h4>
              this is repo named {this.props.data.Project.name}, by {this.props.params.login}, 
              with description: {this.props.data.Project.description}
              Other contributors:
            </h4>
            <ul>
              {this.contributorsList()}
            </ul>
          </div>  
        )
      }
    }
};
const query = gql`
query fetchRepo($login:String $name:String){
  Project(login:$login,name:$name){
    name
    description
    otherContributors{
      id
      login
    }
  }
}
`;
export default graphql(query,{
  options: (props)=>{
    return {variables:{
      login: props.params.login,
      name: props.params.name
    }}
  }
})(RepositoriesPage);